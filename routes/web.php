<?php

use App\Http\Controllers\CartController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PageController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Facades\Voyager;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PageController::class, 'index']);

Route::get('/products/all', [ProductController::class, 'index']);

Route::get('/products/{id}', [ProductController::class, 'show']);

Route::get('/products/category/{id}', [ProductController::class, 'category']);

Route::get('/products/search/{keyword}', [ProductController::class, 'search']);

Route::get('/cart', [CartController::class, 'index']);

Route::post('/cart/{id}', [CartController::class, 'add']);

Route::delete('/cart/{id}', [CartController::class, 'delete']);

Route::delete('/cart', [CartController::class, 'deleteAll']);

Route::put('/cart/{id}', [CartController::class, 'update']);

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
