@extends('layouts.app')

@php
    use App\Category;
    $categories=Category::all();
@endphp

@section('content')
<div class="products-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-4 col-xl-3">
                <ul class="list-unstyled">
                    <h4>Categories</h4>
                    <li>
                        <a class="btn btn-outline-danger col-12 mt-2" href="/products/all">ALL</a>
                    </li>
                    @foreach ($categories as $item)
                        <li class="row">
                            <div class="btn-group col-12 dropright mt-2">
                                <button type="button" class="btn btn-outline-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{$item->name}}
                                </button>
                                <div class="dropdown-menu">
                                    @foreach ($item->subcategories as $sub)
                                        <a class="dropdown-item" href="/products/category/{{$sub->id}}">{{$sub->name}}</a>
                                    @endforeach
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="products-container col-md-12 col-lg-8 col-xl-9 row">
                @if (count($products) > 0)
                    @foreach ($products as $item)
                        <div class="col-md-6 col-lg-4">
                            <div class="product-card my-2 card">
                                <div class="product-card-container">
                                    <a href="/products/{{$item->id}}">
                                        <img src="{{env('APP_URL')}}/storage/{{$item->image}}" alt="product img" class="card-img-top product-card-img" width="100%" height="250px">
                                        <div class="product-card-middle">
                                            <div class="product-card-text">
                                                <i class="fa fa-shopping-bag"></i>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="card-body">
                                    <div class="product-title card-title">
                                        <h1>{{$item->title}}</h1>
                                    </div>
                                    <p class="product-text card-text">Price : {{$item->price}} DH</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <p class="mx-auto">No product</p>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
