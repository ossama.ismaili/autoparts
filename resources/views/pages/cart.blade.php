@php
    use App\Http\Controllers\CartController;
    $total=0;
    if(Auth::check()){
        $total=CartController::totalPrice();
    }
@endphp

@extends('layouts.app')

@section('content')
<div class="cart-section">
    <div class="container">
        <h1>Cart</h1>
        <div class="cart-container">
            @guest
                <div id="cart-guest"></div>
            @else
                @if (count($cart_items) > 0)
                <ul class="list-unstyled">
                    @foreach ($cart_items as $item)
                        <li class="mt-1">
                            <div class="row">
                                <div class="col-md-8 col-lg-10 media">
                                    <img class="mr-3" src="{{env('APP_URL')}}/storage/{{$item->product->image}}" height="70px" width="70px" alt="product picture">
                                    <div class="media-body">
                                        <h5 class="mt-0">{{$item->product->title}}</h5>
                                        <p class="mt-1">Price : {{$item->product->price}} DH</p>
                                    </div>
                                </div>
                                <div class="col-6 col-md-4 col-lg-2">
                                    <form class="mt-2" action="/cart/{{$item->product->id}}" method="post">
                                        {{ csrf_field() }}
                                        {{ method_field('PUT') }}
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <button class="btn btn-secondary" type="submit" value="1" name="decrement">-</button>
                                            </div>
                                            <input type="text" class="form-control text-center" name="quantity" id="product-quantity" value="{{$item->quantity}}">
                                            <div class="input-group-append">
                                                <button class="btn btn-secondary" type="submit" value="-1" name="increment">+</button>
                                            </div>
                                        </div>
                                    </form>
                                    <form class="mt-2" action="/cart/{{$item->product->id}}" method="post">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <div class="btn-group">
                                            <a class="btn btn-success" href="/">Buy</a>
                                            <button class="btn btn-danger" type="submit">delete</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
                <div class="cart-footer mt-3 row">
                    <div class="col-6 mt-3">
                        Totale : {{$total}} DH
                    </div>
                    <div class="col-6 text-right">
                        <form class="mt-2" action="/cart" method="post">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <div class="btn-group">
                                <a class="btn btn-success" href="/">Buy All</a>
                                <button class="btn btn-danger" type="submit">Delete All</button>
                            </div>
                        </form>
                    </div>
                </div>
                @else
                    <p class="text-center">No product in your cart</p>
                @endif
            @endguest
        </div>
    </div>
</div>

@guest
    <script>
        window.onload=function loadCart() {
            if(localStorage.cart){
                if(JSON.parse(localStorage.cart).length > 0){
                    var total=0;
                    var publicUrl=window.location.protocol+"//"+window.location.hostname+":"+window.location.port;
                    var view="<ul class='list-unstyled'>";
                    JSON.parse(localStorage.cart).forEach(
                        (item)=>{
                            total=Number(total)+Number(item.quantity)*Number(item.price);
                            var imageUrl=publicUrl+"/storage/"+item.image;
                            view+="<li class='mt-1'><div class='row'><div class='col-md-8 col-lg-10 media'><img class='mr-3' src='"+imageUrl+"' height='70px' width='70px' alt='product picture'><div class='media-body'><h5 class='mt-0'>"+item.title+"</h5><p class='mt-1'>Price : "+item.price+" DH</p></div></div><div class='col-6 col-md-4 col-lg-2'><div class='input-group'><div class='input-group-prepend'><button class='btn btn-secondary' onclick='decrementQuantity("+item.id+")'>-</button></div><input type='text' class='form-control text-center' name='quantity' id='product-quantity' value='"+item.quantity+"'><div class='input-group-append'><button class='btn btn-secondary' onclick='incrementQuantity("+item.id+")'>+</button></div></div><div class='btn-group mt-2'><a class='btn btn-success' href='/login'>Buy</a><button class='btn btn-danger' onclick='deleteItem("+item.id+")'>delete</button></div></div></div></div></li>";
                        }
                    );
                    view+="</ul>";
                    view+="<div class='cart-footer mt-3 row'><div class='col-6 mt-3'>Totale : "+total.toFixed(2)+" DH</div><div class='col-6 text-right'><div class='btn-group mt-2'><a class='btn btn-success' href='/login'>Buy All</a><button class='btn btn-danger' onclick='deleteAll()'>Delete All</button></div></div></div>"
                    document.getElementById('cart-guest').innerHTML=view;
                }
                else{
                    document.getElementById('cart-guest').innerHTML="<p class='text-center'>No product in your cart</p>";
                }
            }
            else{
                document.getElementById('cart-guest').innerHTML="<p class='text-center'>No product in your cart</p>";
            }
        }

        function saveCart(){
            const exdays=7;
            var date = new Date();
            date.setTime(date.getTime() + (exdays*24*60*60*1000));
            var expires = "expires="+ date.toUTCString();
            document.cookie = "cart=" + localStorage.cart + ";" + expires + ";path=/cart";
        }

        function deleteItem(id) {
            var guestCart=JSON.parse(localStorage.cart);
            guestCart=guestCart.filter(item=>item.id!=id);
            localStorage.cart=JSON.stringify(guestCart);
            saveCart();
            location.reload();
        }

        function deleteAll() {
            var guestCart=[];
            localStorage.cart=JSON.stringify(guestCart);
            saveCart();
            location.reload();
        }

        function incrementQuantity(id) {
            var guestCart=JSON.parse(localStorage.cart);
            guestCart.forEach((item) => {
                if(item.id==id){
                    item.quantity=Number(item.quantity)+1;
                }
            });
            localStorage.cart=JSON.stringify(guestCart);
            saveCart();
            location.reload();
        }

        function decrementQuantity(id) {
            var guestCart=JSON.parse(localStorage.cart);
            guestCart.forEach((item) => {
                if(item.id==id){
                    item.quantity=Number(item.quantity)-1;
                }
            });
            localStorage.cart=JSON.stringify(guestCart);
            saveCart();
            location.reload();
        }
    </script>
@endguest

@endsection
