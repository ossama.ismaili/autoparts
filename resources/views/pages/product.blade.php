
@extends('layouts.app')

@section('content')
<div class="product-section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>{{$product->title}}</h2>
            </div>
        </div>
        <div class="row">
            <img src="{{env('APP_URL')}}/storage/{{$product->image}}" alt="product img" class="col-md-4" />
            <div class="col-md-8">
                <div class="row">
                    <div class="col-12 mt-3">
                        Price <span class="float-right">{{$product->price}} DH</span>
                    </div>
                    <div class="col-12 mt-3">
                        Category <span class="float-right">{{$product->subcategory->name}}</span>
                    </div>
                    @guest
                        <div class="col-12 mt-3">
                            Quantity <span class="float-right"><input type="number" class="text-center" value="1" id="quantity" min="1"></span>
                        </div>
                        <div class="col-lg-6 mt-3 p-0">
                            <div class="btn-group col-12" role="group" aria-label="Options">
                                <a class="btn btn-success text-left" href="{{env('APP_URL').'/login'}}"><span class="fa fa-money mr-2"></span> Buy now</a>
                                <button class="btn btn-primary text-left" onclick="addToCart({{$product->id}}, '{{$product->title}}',  String.raw`{{$product->image}}`, {{$product->price}})"><span class="fa fa-shopping-cart mr-2"></span> Add to cart</button>
                            </div>
                        </div>
                    @else
                    <form class="col-12" action="/cart/{{$product->id}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('POST') }}
                        <div class="row">
                            <div class="col-12 mt-3">
                                Quantity <span class="float-right"><input type="number" class="text-center" value="1" name="quantity" min="1"></span>
                            </div>
                            <div class="col-lg-6 mt-3 p-0">
                                <div class="btn-group col-12" role="group" aria-label="Options">
                                    <a class="btn btn-success text-left"><span class="fa fa-money mr-2"></span> Buy now</a>
                                    <button class="btn btn-primary text-left" type="submit"><span class="fa fa-shopping-cart mr-2"></span> Add to cart</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    @endguest
                </div>
            </div>
            <div class="col-12 mt-4">
                <h3>Description : </h3>
                <p>{!! $product->description !!}</p>
            </div>
        </div>
    </div>
</div>
<script>
    function saveCart() {
        const exdays=10;
        var date = new Date();
        date.setTime(date.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ date.toUTCString();
        document.cookie = "cart=" + localStorage.cart + ";" + expires + ";path=/cart";
    }

    function addToCart(id, title, image, price) {
        if(localStorage.cart){
            var exist=false;
            var quantity=document.getElementById('quantity').value;
            var guestCart=JSON.parse(localStorage.cart);
            guestCart.forEach((item) => {
                if(item.id==id){
                    item.quantity=Number(item.quantity)+Number(quantity);
                    exist=true;
                }
            });
            if(!exist){
                guestCart.push({
                    id:id,
                    title:title,
                    image:image,
                    price:price,
                    quantity:quantity
                });
            }
            localStorage.cart=JSON.stringify(guestCart);
        }
        else{
            var quantity=document.getElementById('quantity').value;
            localStorage.cart=JSON.stringify([{
                id:id,
                title:title,
                image:image,
                price:price,
                quantity:quantity
            }]);
        }
        saveCart();
        window.location.replace("/cart");
    }
</script>
@endsection
