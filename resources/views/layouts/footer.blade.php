 @php
    use App\Category;
    $categories=Category::all();
 @endphp
 <!-- Footer Container -->
 <footer class="footer-container">
    <div class="row-dark">
        <div class="row container container-top">
            <div class="col-lg-6">
                <ul class="list-unstyled socials">
                    <li class="facebook"><span class="social-element"><a href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a></span></li>
                    <li class="twitter"><span class="social-element"><a href="https://twitter.com/s" target="_blank"><i class="fa fa-twitter"></i></a></span></li>
                    <li class="google_plus"><span class="social-element"><a href="https://plus.google.com/" target="_blank"><i class="fa fa-google-plus"></i></a></span></li>
                    <li class="pinterest"><span class="social-element"><a href="https://www.pinterest.com/" target="_blank"><i class="fa fa-pinterest-p"></i></a></span></li>
                    <li class="instagram"><span class="social-element"><a href="#" target="_blank"><i class="fa fa-instagram"></i></a></span></li>
                    <li class="Youtube"><span class="social-element"><a href="#" target="_blank"><i class="fa fa-youtube-play"></i></a></span></li>
                </ul>
            </div>
            <div class="col-lg-6">
                <div class="module newsletter-footer1">
                    <div class="newsletter">
                        <h3 class="modtitle">Sign Up for Newsletter</h3>
                        <div class="block_content">
                            <form method="post" id="signup" name="signup" class="form-group signup send-mail">
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <input type="email" placeholder="Your email address..." value="" class="form-control" id="txtemail" name="txtemail" size="55">
                                        <div class="input-group-append">
                                            <button class="btn btn-dark btn-default font-title" type="submit" onclick="return subscribe_newsletter();" name="submit">
                                                <span>Subscribe</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row footer-middle">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-style">
                <div class="box-footer box-infos">
                    <div class="module">
                        <h3 class="modtitle">Contact us</h3>
                        <div class="modcontent">
                            <ul class="list-icon list-unstyled">
                                <li><span class="icon pe-7s-map-marker"></span>5611 Wellington Road, Suite 115, Gainesville, VA 20155</li>
                                <li><span class="icon pe-7s-call"></span> <a href="#">888 9344 6000 - 888 1234 6789</a></li>
                                <li><span class="icon pe-7s-mail"></span><a href="mailto:contact@autoparts.com">contact@autoparts.com</a></li>
                                <li><span class="icon pe-7s-alarm"></span>7 Days a week from 10-00 am to 6-00 pm</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-style">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 col-style">
                        <div class="box-information box-footer">
                            <div class="module clearfix">
                                <h3 class="modtitle">Information</h3>
                                <div class="modcontent">
                                    <ul class="menu list-unstyled ">
                                        <li><a href="#">About Us</a></li>
                                        <li><a href="#">FAQ</a></li>
                                        <li><a href="#">Warranty And Services</a></li>
                                        <li><a href="#">Support 24/7 page</a></li>
                                        <li><a href="#">Product Registration</a></li>
                                        <li><a href="#">Product Support</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 col-style">
                        <div class="box-account box-footer">
                            <div class="module clearfix">
                                <h3 class="modtitle">My Account</h3>
                                <div class="modcontent">
                                    <ul class="menu list-unstyled ">
                                        <li><a href="#">Brands</a></li>
                                        <li><a href="#">Gift Certificates</a></li>
                                        <li><a href="#">Affiliates</a></li>
                                        <li><a href="#">Specials</a></li>
                                        <li><a href="#">FAQs</a></li>
                                        <li><a href="#">Custom Link</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 col-clear">
                        <div class="box-service box-footer">
                          <div class="module clearfix">
                            <h3 class="modtitle">Categories</h3>
                            <div class="modcontent">
                              <ul class="menu list-unstyled ">
                                @foreach ($categories as $item)
                                    <li>
                                        <h6 class="mt-2">{{$item->name}}</h6>
                                        <ul class="list-unstyled ml-3">
                                            @foreach ($item->subcategories as $sub)
                                                <li>
                                                    <a href="/products/category/{{$sub->id}}">{{$sub->name}}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                @endforeach
                              </ul>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="payment-w col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <img src="image/catalog/demo/payment/payment.png" alt="imgpayment">
                </div>
                <div class="copyright col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <p>Autoparts © 2020. All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>
