<header class="header-container">
    <nav class="navbar navbar-expand-lg navbar-dark">
        <a class="navbar-brand" href="/"><img src="{{asset('images/icon.png')}}" width="25px" alt="iconimg"> <strong><em>AUTOPARTS</em></strong></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                @guest
                    <li class="nav-item mr-2">
                        <a class="nav-link" href="/cart">
                            <span class="icon-container">
                                <i class="fa fa-shopping-cart"></i>
                            </span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}"><span class="fa fa-lock text-danger mr-2"></span>{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item">
                        <a class="nav-link" href="/cart">
                            <span class="icon-container">
                                <i class="fa fa-shopping-cart"></i>
                            </span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }}
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                <span class="fa fa-sign-out"></span>
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </nav>
    <div class="jumbotron">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-12 text-sm-center">
                <div class="logo">
                    <a href="/"><img src="{{asset('images/logo.png')}}" title="Your Store" alt="Your Store"></a>
                </div>
            </div>

            <div class="col-lg-8 col-md-6 mt-2 row">
                <div class="input-group col-10 ml-lg-auto mx-sm-auto mb-3">
                    <input type="text" class="form-control" placeholder="Search" aria-label="search" aria-describedby="search input" id="searchInput" onkeypress="if(event.key === 'Enter')document.getElementById('searchBtn').click();" required>
                    <div class="input-group-append">
                      <button class="btn btn-outline-danger" type="button" onclick="if(document.getElementById('searchInput').value.length>0) window.location.replace('/products/search/'+document.getElementById('searchInput').value);" id="searchBtn" ><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
