-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 08, 2020 at 10:33 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `autoparts`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart_items`
--

CREATE TABLE `cart_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cart_items`
--

INSERT INTO `cart_items` (`id`, `user_id`, `product_id`, `quantity`, `created_at`, `updated_at`) VALUES
(6, 1, 5, 1, '2020-10-08 19:21:44', '2020-10-08 19:21:44'),
(7, 1, 7, 1, '2020-10-08 19:21:58', '2020-10-08 19:21:58');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`, `description`, `image`) VALUES
(1, 'Replacement Parts', '2020-09-24 11:07:09', '2020-09-24 11:27:54', 'Find the right filters, brakes, lighting, mirrors, spark plugs, & more', 'categories\\September2020\\uGPA9OQtMwizHKC6Gj3u.png'),
(2, 'Performance Parts & Accessories', '2020-09-24 11:09:51', '2020-09-24 11:27:18', 'Find the right suspension, exhaust, brakes, filters, batteries, & more', 'categories\\September2020\\Hv6wKbXIa0a0hLEnsO1t.png');

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(62, 8, 'id', 'number', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(63, 8, 'user_id', 'number', 'User Id', 1, 1, 1, 1, 1, 0, '{}', 2),
(65, 8, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 0, '{}', 4),
(66, 8, 'description', 'text_area', 'Description', 1, 1, 1, 1, 1, 0, '{}', 5),
(67, 8, 'price', 'number', 'Price', 1, 1, 1, 1, 1, 0, '{}', 6),
(68, 8, 'image', 'image', 'Image', 1, 1, 1, 1, 1, 1, '{}', 7),
(69, 8, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 8),
(70, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(71, 8, 'product_belongsto_user_relationship', 'relationship', 'users', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 10),
(80, 10, 'id', 'number', 'Id', 1, 1, 0, 0, 0, 0, '{}', 1),
(81, 10, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{}', 2),
(82, 10, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 3),
(83, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(85, 8, 'category_id', 'text', 'Category Id', 0, 1, 1, 1, 1, 1, '{}', 9),
(86, 10, 'description', 'text', 'Description', 0, 1, 1, 1, 1, 1, '{}', 5),
(87, 10, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 6),
(88, 11, 'id', 'number', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(89, 11, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 3),
(90, 11, 'description', 'text_area', 'Description', 0, 1, 1, 1, 1, 1, '{}', 4),
(91, 11, 'image', 'image', 'Image', 1, 1, 1, 1, 1, 1, '{}', 5),
(92, 11, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 6),
(93, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(94, 11, 'category_id', 'number', 'Category Id', 0, 1, 1, 1, 1, 1, '{}', 2),
(95, 11, 'subcategory_belongsto_category_relationship', 'relationship', 'categories', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Category\",\"table\":\"categories\",\"type\":\"belongsTo\",\"column\":\"category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 8),
(96, 8, 'product_belongsto_subcategory_relationship', 'relationship', 'subcategories', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Subcategory\",\"table\":\"subcategories\",\"type\":\"belongsTo\",\"column\":\"category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":null}', 11),
(97, 12, 'id', 'number', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(98, 12, 'user_id', 'number', 'User Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(99, 12, 'product_id', 'number', 'Product Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(100, 12, 'quantity', 'number', 'Quantity', 1, 1, 1, 1, 1, 1, '{}', 4),
(101, 12, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 5),
(102, 12, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(103, 12, 'cart_item_belongsto_user_relationship', 'relationship', 'users', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"cart_items\",\"pivot\":\"0\",\"taggable\":\"0\"}', 7),
(104, 12, 'cart_item_belongsto_product_relationship', 'relationship', 'products', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Product\",\"table\":\"products\",\"type\":\"belongsTo\",\"column\":\"product_id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"cart_items\",\"pivot\":\"0\",\"taggable\":\"0\"}', 8);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2020-09-18 15:01:04', '2020-09-18 15:01:04'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2020-09-18 15:01:04', '2020-09-18 15:01:04'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2020-09-18 15:01:04', '2020-09-18 15:01:04'),
(8, 'products', 'products', 'Product', 'Products', NULL, 'App\\Product', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-09-18 15:09:05', '2020-10-08 16:22:18'),
(10, 'categories', 'categories', 'Category', 'Categories', NULL, 'App\\Category', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-09-24 10:54:18', '2020-09-24 11:21:18'),
(11, 'subcategories', 'subcategories', 'Subcategory', 'Subcategories', NULL, 'App\\Subcategory', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-10-08 15:55:11', '2020-10-08 16:09:50'),
(12, 'cart_items', 'cart-items', 'Cart Item', 'Cart Items', NULL, 'App\\CartItem', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-10-08 18:12:38', '2020-10-08 18:14:55');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2020-09-18 15:01:07', '2020-09-18 15:01:07');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-home', '#000000', NULL, 1, '2020-09-18 15:01:07', '2020-09-18 15:32:19', 'voyager.dashboard', 'null'),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 4, '2020-09-18 15:01:07', '2020-09-18 15:19:26', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2020-09-18 15:01:07', '2020-09-18 15:01:07', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2020-09-18 15:01:07', '2020-09-18 15:01:07', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 7, '2020-09-18 15:01:07', '2020-10-08 15:58:19', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2020-09-18 15:01:07', '2020-09-18 15:19:26', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2020-09-18 15:01:07', '2020-09-18 15:19:26', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2020-09-18 15:01:07', '2020-09-18 15:19:26', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2020-09-18 15:01:07', '2020-09-18 15:19:26', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 9, '2020-09-18 15:01:07', '2020-10-08 18:15:07', 'voyager.settings.index', NULL),
(14, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2020-09-18 15:01:22', '2020-09-18 15:19:26', 'voyager.hooks', NULL),
(16, 1, 'Products', '', '_self', 'voyager-bag', '#000000', NULL, 5, '2020-09-18 15:09:05', '2020-09-24 10:54:46', 'voyager.products.index', 'null'),
(18, 1, 'Categories', '', '_self', 'voyager-archive', '#000000', 20, 1, '2020-09-24 10:54:18', '2020-10-08 15:58:19', 'voyager.categories.index', 'null'),
(19, 1, 'Subcategories', '', '_self', 'voyager-archive', '#000000', 20, 2, '2020-10-08 15:55:12', '2020-10-08 15:58:20', 'voyager.subcategories.index', 'null'),
(20, 1, 'Ranges', '', '_self', 'voyager-categories', '#000000', NULL, 6, '2020-10-08 15:58:07', '2020-10-08 16:16:44', NULL, ''),
(21, 1, 'Cart Items', '', '_self', 'voyager-basket', '#000000', NULL, 8, '2020-10-08 18:12:39', '2020-10-08 18:15:29', 'voyager.cart-items.index', 'null');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_01_01_000000_create_pages_table', 1),
(6, '2016_01_01_000000_create_posts_table', 1),
(7, '2016_02_15_204651_create_categories_table', 1),
(8, '2016_05_19_173453_create_menu_table', 1),
(9, '2016_10_21_190000_create_roles_table', 1),
(10, '2016_10_21_190000_create_settings_table', 1),
(11, '2016_11_30_135954_create_permission_table', 1),
(12, '2016_11_30_141208_create_permission_role_table', 1),
(13, '2016_12_26_201236_data_types__add__server_side', 1),
(14, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(15, '2017_01_14_005015_create_translations_table', 1),
(16, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(17, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(18, '2017_04_11_000000_alter_post_nullable_fields_table', 1),
(19, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(20, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(21, '2017_08_05_000000_add_group_to_settings_table', 1),
(22, '2017_11_26_013050_add_user_role_relationship', 1),
(23, '2017_11_26_015000_create_user_roles_table', 1),
(24, '2018_03_11_000000_add_user_settings', 1),
(25, '2018_03_14_000000_add_details_to_data_types_table', 1),
(26, '2018_03_16_000000_make_settings_value_nullable', 1),
(27, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2020-09-18 15:01:08', '2020-09-18 15:01:08'),
(2, 'browse_bread', NULL, '2020-09-18 15:01:08', '2020-09-18 15:01:08'),
(3, 'browse_database', NULL, '2020-09-18 15:01:08', '2020-09-18 15:01:08'),
(4, 'browse_media', NULL, '2020-09-18 15:01:08', '2020-09-18 15:01:08'),
(5, 'browse_compass', NULL, '2020-09-18 15:01:08', '2020-09-18 15:01:08'),
(6, 'browse_menus', 'menus', '2020-09-18 15:01:08', '2020-09-18 15:01:08'),
(7, 'read_menus', 'menus', '2020-09-18 15:01:08', '2020-09-18 15:01:08'),
(8, 'edit_menus', 'menus', '2020-09-18 15:01:08', '2020-09-18 15:01:08'),
(9, 'add_menus', 'menus', '2020-09-18 15:01:08', '2020-09-18 15:01:08'),
(10, 'delete_menus', 'menus', '2020-09-18 15:01:08', '2020-09-18 15:01:08'),
(11, 'browse_roles', 'roles', '2020-09-18 15:01:08', '2020-09-18 15:01:08'),
(12, 'read_roles', 'roles', '2020-09-18 15:01:08', '2020-09-18 15:01:08'),
(13, 'edit_roles', 'roles', '2020-09-18 15:01:08', '2020-09-18 15:01:08'),
(14, 'add_roles', 'roles', '2020-09-18 15:01:08', '2020-09-18 15:01:08'),
(15, 'delete_roles', 'roles', '2020-09-18 15:01:08', '2020-09-18 15:01:08'),
(16, 'browse_users', 'users', '2020-09-18 15:01:09', '2020-09-18 15:01:09'),
(17, 'read_users', 'users', '2020-09-18 15:01:09', '2020-09-18 15:01:09'),
(18, 'edit_users', 'users', '2020-09-18 15:01:09', '2020-09-18 15:01:09'),
(19, 'add_users', 'users', '2020-09-18 15:01:09', '2020-09-18 15:01:09'),
(20, 'delete_users', 'users', '2020-09-18 15:01:09', '2020-09-18 15:01:09'),
(21, 'browse_settings', 'settings', '2020-09-18 15:01:09', '2020-09-18 15:01:09'),
(22, 'read_settings', 'settings', '2020-09-18 15:01:09', '2020-09-18 15:01:09'),
(23, 'edit_settings', 'settings', '2020-09-18 15:01:09', '2020-09-18 15:01:09'),
(24, 'add_settings', 'settings', '2020-09-18 15:01:09', '2020-09-18 15:01:09'),
(25, 'delete_settings', 'settings', '2020-09-18 15:01:09', '2020-09-18 15:01:09'),
(41, 'browse_hooks', NULL, '2020-09-18 15:01:22', '2020-09-18 15:01:22'),
(47, 'browse_products', 'products', '2020-09-18 15:09:05', '2020-09-18 15:09:05'),
(48, 'read_products', 'products', '2020-09-18 15:09:05', '2020-09-18 15:09:05'),
(49, 'edit_products', 'products', '2020-09-18 15:09:05', '2020-09-18 15:09:05'),
(50, 'add_products', 'products', '2020-09-18 15:09:05', '2020-09-18 15:09:05'),
(51, 'delete_products', 'products', '2020-09-18 15:09:05', '2020-09-18 15:09:05'),
(57, 'browse_categories', 'categories', '2020-09-24 10:54:18', '2020-09-24 10:54:18'),
(58, 'read_categories', 'categories', '2020-09-24 10:54:18', '2020-09-24 10:54:18'),
(59, 'edit_categories', 'categories', '2020-09-24 10:54:18', '2020-09-24 10:54:18'),
(60, 'add_categories', 'categories', '2020-09-24 10:54:18', '2020-09-24 10:54:18'),
(61, 'delete_categories', 'categories', '2020-09-24 10:54:18', '2020-09-24 10:54:18'),
(62, 'browse_subcategories', 'subcategories', '2020-10-08 15:55:11', '2020-10-08 15:55:11'),
(63, 'read_subcategories', 'subcategories', '2020-10-08 15:55:11', '2020-10-08 15:55:11'),
(64, 'edit_subcategories', 'subcategories', '2020-10-08 15:55:11', '2020-10-08 15:55:11'),
(65, 'add_subcategories', 'subcategories', '2020-10-08 15:55:11', '2020-10-08 15:55:11'),
(66, 'delete_subcategories', 'subcategories', '2020-10-08 15:55:11', '2020-10-08 15:55:11'),
(67, 'browse_cart_items', 'cart_items', '2020-10-08 18:12:39', '2020-10-08 18:12:39'),
(68, 'read_cart_items', 'cart_items', '2020-10-08 18:12:39', '2020-10-08 18:12:39'),
(69, 'edit_cart_items', 'cart_items', '2020-10-08 18:12:39', '2020-10-08 18:12:39'),
(70, 'add_cart_items', 'cart_items', '2020-10-08 18:12:39', '2020-10-08 18:12:39'),
(71, 'delete_cart_items', 'cart_items', '2020-10-08 18:12:39', '2020-10-08 18:12:39');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(41, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `user_id`, `title`, `description`, `price`, `image`, `created_at`, `updated_at`, `category_id`) VALUES
(2, 1, 'Ginsco 102pcs 6.3mm 8mm 9mm 10mm Nylon Bumper Push Fasteners Rivet Clips Expansion Screws Replacement Kit', 'All packed in a plastic container for easy organization and storage.\r\n4 hole sizes from 6.3mm(1/4\"), 8mm(5/16\" ), 9mm(23/64\" ), 10mm(25/64\" ).\r\n6 different push pin rivet for ATV & many brand vehicles.\r\nGreat for Door Trim, Car Fender, Bumper, Side Skirt, Radiator Shield, Splash Shield Retainers', 75, 'products\\September2020\\GT7ZTMRRMdWbijrUaWrq.jpg', '2020-09-18 17:13:59', '2020-10-08 16:23:08', 2),
(3, 1, '3M Headlight Lens Restoration System, 39008', 'SEE BETTER AT NIGHT: Enhances visibility and safety for night driving\r\nEasy, 3-step process requires as little as an hour to remove scratches and restore cloudy, dull lenses\r\nFor use on plastic lenses headlights, taillights, fog lights, directional lights and more\r\n3M abrasive technology removes yellowing and buildup without damaging plastic lens surface\r\nEasy, 3-step process requires as little as an hour to remove scratches and restore cloudy, dull lenses\r\nFeatures 3M abrasive technology and a polishing compound to restore hazy and dull headlight lenses, as well as other plastic lens surfaces on your vehicle\r\nSystem is intended to restore two headlight lenses', 110, 'products\\September2020\\pxhTigxl6yMvkaJrTlAs.jpg', '2020-09-18 17:16:35', '2020-09-24 11:11:06', 2),
(4, 1, 'Power Stop K2813-36 Front & Rear Z36 Truck and Tow Brake Kit', 'Proprietary carbon-fiber ceramic materials enhance the strength of a typical ceramic brake pad compound to handle heavier loads\r\nLow dust braking validated through on-vehicle 3rd party tests in Los Angeles, showing on average 30% less dust versus OE\r\nDrilled & Slotted performance rotors for maximum cooling\r\nIncludes stainless steel installation hardware and high temperature ceramic brake lubricant\r\nFitment: Lexus LX570 2018-2016; Toyota Land Cruiser 2019-2016; Sequoia 2019-2008; Tundra 2019-2007', 3864.55, 'products\\September2020\\Ehy7uc4ZpG4iMJzZF7Qt.jpg', '2020-09-18 17:17:54', '2020-10-08 16:38:22', 3),
(5, 1, 'EBC Brakes DP41774R Yellowstuff Street and Track Brake Pad', 'High friction pad for better high speed stopping\r\nZero fade race developed compound\r\nBrakes effective right from cold\r\nCan be used on street or race track', 1034.99, 'products\\September2020\\8UjfAagAlHBR4xc61AjS.jpg', '2020-09-18 17:21:09', '2020-09-24 11:10:30', 1),
(6, 1, 'Oil Pump, Wet Sump, Internal, Standard Volume, GM LS-Series, Each', 'Available at a lower price from other sellers that may not offer free Prime shipping.\r\n    Country of origin : United States\r\n    Package Dimensions : 7.98\" L x 5.87\" W x 5.1\" H\r\n    Package weight : 2 pounds\r\n    Model number : M295', 559.99, 'products\\October2020\\SrIRFcXi30Y3zv0AhYVe.jpg', '2020-10-08 16:46:02', '2020-10-08 16:46:02', 4),
(7, 1, 'Turbosmart TS-0203-1061 Blow-Off Valve', 'Available at a lower price from other sellers that may not offer free Prime shipping.\r\n    They also fit the popular Ford Eco boost engines.\r\n    The package length is 15.1 inches\r\n    The package height is 11.2 inches\r\n    The package width is 11.3 inches', 1482.49, 'products\\October2020\\rtD6xoscbq1bjzBo8F5U.jpg', '2020-10-08 16:47:17', '2020-10-08 16:47:17', 4);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2020-09-18 15:01:07', '2020-09-18 15:01:07'),
(2, 'user', 'Normal User', '2020-09-18 15:01:07', '2020-09-18 15:01:07');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', 'settings\\September2020\\y1au44E4iDis5n9HFbGE.jpg', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Dashboard', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Autoparts Dashboard', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', 'settings\\September2020\\z5RcDLYIdqjlxMS0xdGi.png', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE `subcategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`id`, `name`, `description`, `image`, `created_at`, `updated_at`, `category_id`) VALUES
(1, 'Brake System', NULL, 'subcategories\\October2020\\5Us9EBUCBYVbNZdN1EdP.jpg', '2020-10-08 16:10:39', '2020-10-08 16:10:39', 1),
(2, 'Body and Trim', NULL, 'subcategories\\October2020\\WkO3oMcp2A8TTaaPHgOm.jpg', '2020-10-08 16:11:09', '2020-10-08 16:11:09', 1),
(3, 'Brake System', NULL, 'subcategories\\October2020\\nDQiAkYKimH1znfFVafM.jpg', '2020-10-08 16:19:32', '2020-10-08 16:19:32', 2),
(4, 'Engines and Engine Parts', NULL, 'subcategories\\October2020\\PV3JwxopN7NvdSe8bEji.jpg', '2020-10-08 16:20:02', '2020-10-08 16:20:02', 2);

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2020-09-18 15:01:18', '2020-09-18 15:01:18'),
(2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2020-09-18 15:01:18', '2020-09-18 15:01:18'),
(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2020-09-18 15:01:19', '2020-09-18 15:01:19'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2020-09-18 15:01:19', '2020-09-18 15:01:19'),
(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2020-09-18 15:01:19', '2020-09-18 15:01:19'),
(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2020-09-18 15:01:19', '2020-09-18 15:01:19'),
(7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2020-09-18 15:01:19', '2020-09-18 15:01:19'),
(8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2020-09-18 15:01:19', '2020-09-18 15:01:19'),
(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2020-09-18 15:01:19', '2020-09-18 15:01:19'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2020-09-18 15:01:19', '2020-09-18 15:01:19'),
(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2020-09-18 15:01:19', '2020-09-18 15:01:19'),
(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2020-09-18 15:01:19', '2020-09-18 15:01:19'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2020-09-18 15:01:19', '2020-09-18 15:01:19'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2020-09-18 15:01:19', '2020-09-18 15:01:19'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2020-09-18 15:01:19', '2020-09-18 15:01:19'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2020-09-18 15:01:19', '2020-09-18 15:01:19'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2020-09-18 15:01:19', '2020-09-18 15:01:19'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2020-09-18 15:01:20', '2020-09-18 15:01:20'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2020-09-18 15:01:20', '2020-09-18 15:01:20'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2020-09-18 15:01:20', '2020-09-18 15:01:20'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2020-09-18 15:01:20', '2020-09-18 15:01:20'),
(22, 'menu_items', 'title', 12, 'pt', 'Publicações', '2020-09-18 15:01:20', '2020-09-18 15:01:20'),
(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2020-09-18 15:01:20', '2020-09-18 15:01:20'),
(24, 'menu_items', 'title', 11, 'pt', 'Categorias', '2020-09-18 15:01:20', '2020-09-18 15:01:20'),
(25, 'menu_items', 'title', 13, 'pt', 'Páginas', '2020-09-18 15:01:20', '2020-09-18 15:01:20'),
(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2020-09-18 15:01:20', '2020-09-18 15:01:20'),
(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2020-09-18 15:01:20', '2020-09-18 15:01:20'),
(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2020-09-18 15:01:20', '2020-09-18 15:01:20'),
(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2020-09-18 15:01:20', '2020-09-18 15:01:20'),
(30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2020-09-18 15:01:20', '2020-09-18 15:01:20');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin', 'admin@admin.com', 'users\\September2020\\Phn8KrpTe87xHYFjJEGW.png', NULL, '$2y$10$wGIKNLvEskA16MBU9yoYwuTVdYx9yjvjXL.N1OdiHkZxohwtUc5Cq', 'FUXodnxNZRTbIlYjEfIKmOqoWmNNQRZMiAnSsH7VIfjPWg7ZGskOxymzRSeZ', '{\"locale\":\"en\"}', '2020-09-18 15:01:14', '2020-09-18 15:39:47'),
(2, 2, 'Normal User', 'user@user.com', 'users\\September2020\\qXRGna38ofutprYBoWSw.png', NULL, '$2y$10$AcKS2UhslWg9h.VOx42Q6uuE9TYffGBILtsyne2Z8Vi2If.gYJHU.', NULL, '{\"locale\":\"en\"}', '2020-09-24 12:51:06', '2020-09-24 12:59:38');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart_items`
--
ALTER TABLE `cart_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart_items`
--
ALTER TABLE `cart_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `subcategories`
--
ALTER TABLE `subcategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
