<?php

namespace App\Http\Controllers;

use App\CartItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    public function index()
    {
        if(Auth::check())
        {
            $cart_items=CartItem::where('user_id',Auth::user()->id)->orderBy('created_at', 'desc')->get();
            return view('pages.cart')->with('cart_items',$cart_items);
        }
        else{
            return view('pages.cart');
        }
    }

    public function add(Request $request, $product_id)
    {
        $quantity=$request->input('quantity');
        $check=CartItem::where('user_id',Auth::user()->id)->where('product_id',$product_id)->get();

        if(count($check) > 0 && $quantity > 0){
            $current_quantity=CartItem::where('user_id',Auth::user()->id)->where('product_id',$product_id)->value('quantity');
            CartItem::where('user_id',Auth::user()->id)->where('product_id',$product_id)->update(['quantity'=>$current_quantity+$quantity]);
        }
        else if($quantity > 0){
            $item=CartItem::create([
                'user_id' => Auth::user()->id,
                'product_id' => $product_id,
                'quantity' => $quantity,
            ]);

            $item->save();
        }
        else{
            abort(400);
        }

        return redirect('/cart');
    }

    public function update(Request $request,$product_id)
    {
        $inc=$request->input('increment');
        $dec=$request->input('decrement');

        if(isset($inc)){
            $quantity=CartItem::where('user_id',Auth::user()->id)->where('product_id',$product_id)->value('quantity');
            CartItem::where('user_id',Auth::user()->id)->where('product_id',$product_id)->update(['quantity'=>$quantity+1]);
        }
        else if(isset($dec)){
            $quantity=CartItem::where('user_id',Auth::user()->id)->where('product_id',$product_id)->value('quantity');
            CartItem::where('user_id',Auth::user()->id)->where('product_id',$product_id)->update(['quantity'=>$quantity-1]);
        }

        return redirect('/cart');
    }

    public function delete($product_id)
    {
        CartItem::where('user_id',Auth::user()->id)->where('product_id',$product_id)->delete();
        return redirect('/cart');
    }

    public function deleteAll()
    {
        CartItem::where('user_id', Auth::user()->id)->delete();
        return redirect('/cart');
    }

    public static function totalPrice()
    {
        $items=CartItem::where('user_id',Auth::user()->id)->get();
        $total=0;
        foreach($items as $item){
            $total = $total + $item->product->price * $item->quantity;
        }
        return $total;
    }
}
